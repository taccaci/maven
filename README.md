TACC Maven Repo
=====================

This contains some of the older libs used by our projects that are unlikely or not hosted elsewhere. To use this repo, try including it in your pom as:

	<repositories>
		<repository>
			<id>taccaci</id>
			<url>https://bitbucket.org/taccaci/maven/raw/main</url>
		</repository>
	</repositories>